<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-22
 * Time: 08:44
 */

namespace SDA\Rafal\Entities;


use SDA\Rafal\Interfaces\EntityInterface;
use SDA\Rafal\ValueObjects\WordValueObject;

class RequestWordInsertEntity implements EntityInterface
{
    protected $word;
    protected $translate;

    public function __construct()
    {
    }

    /**
     * @return WordValueObject
     */
    public function getWord(): WordValueObject
    {
        return $this->word;
    }

    /**
     * @param WordValueObject $word
     * @return RequestWordInsertEntity
     */
    public function setWord(WordValueObject $word): RequestWordInsertEntity
    {
        $this->word = $word;
        return $this;
    }

    /**
     * @return WordValueObject
     */
    public function getTranslate(): WordValueObject
    {
        return $this->translate;
    }

    /**
     * @param WordValueObject $translate
     * @return RequestWordInsertEntity
     */
    public function setTranslate(WordValueObject $translate): RequestWordInsertEntity
    {
        $this->translate = $translate;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['word' => $this->word,
            'translate' => $this->translate
        ];
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return ['word', 'translate'];
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        if (isset($_SESSION['login']))
            return $_SESSION['login'];
    }

    /**
     * @param array $params
     * @return EntityInterface
     */
    public function prepareFromArray(array $params): EntityInterface
    {
        if (isset($params['word'])){ try{ $this->setWord(new WordValueObject($params['word'])); } catch (\TypeError $exception){} }
        if (isset($params['translate'])){ try{ $this->setWord(new WordValueObject($params['translate'])); } catch (\TypeError $exception){} }

        return $this;
    }

    public function isValid(): bool
    {
        $isValid = true;
        if(empty($this->getWord())) $isValid = false;
        if(empty($this->getTranslate())) $isValid = false;

        return $isValid;
    }

    public function getIdName(): string
    {
        // TODO: Implement getIdName() method.
    }
}