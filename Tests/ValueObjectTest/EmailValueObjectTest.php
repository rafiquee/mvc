<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 21:35
 */

namespace SDA\Rafal\Tests\ValueObjectTest;


use PHPUnit\Framework\TestCase;
use SDA\Rafal\ValueObjects\EmailValueObject;

class EmailValueObjectTest extends TestCase
{
    /** @test */
    public function IfIcanCreateEmailWithCorrectAddress(){

        return  $this->assertEquals('test@test.pl',(new EmailValueObject('test@test.pl'))->get());

    }

    public function testIfMagicalFunctionWorksCorrectly()
    {
        $expected = 'test@test.pl';
        $email = new EmailValueObject($expected);
        $actual =$email->get();

        $this->assertEquals($expected, (string)$actual);
    }
}