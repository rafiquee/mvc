<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-22
 * Time: 08:42
 */

namespace SDA\Rafal\TransportObjects;


use SDA\Rafal\Interfaces\TransportObjectsInterface;
use SDA\Rafal\ValueObjects\WordValueObject;

class RequestWordInsert implements TransportObjectsInterface
{

    private $word;
    private $translate;

    public function __construct()
    {
    }

    /**
     * @return WordValueObject
     */
    public function getTranslate(): WordValueObject
    {
        return $this->translate;
    }

    /**
     * @param WordValueObject $translate
     * @return RequestWordInsert
     */
    public function setTranslate(WordValueObject $translate): RequestWordInsert
    {
        $this->translate = $translate;
        return $this;
    }

    /**
     * @return WordValueObject
     */
    public function getWord(): WordValueObject
    {
        return $this->word;
    }

    /**
     * @param WordValueObject $word
     * @return RequestWordInsert
     */
    public function setWord(WordValueObject $word): RequestWordInsert
    {
        $this->word = $word;
        return $this;
    }

    public function isValid(): bool
    {
        $status = true;
        if(empty($this->getWord())) $status = false;
        if(empty($this->getTranslate())) $status = false;

        return $status;
    }

    /**
     * @param array $params
     * @return TransportObjectsInterface
     */
    public function prepareFromArray(array $params): TransportObjectsInterface
    {
        if (isset($params['word']))
        { try
            {
                $this->setWord(new WordValueObject($params['word']));
            }
            catch (\TypeError $exception){}
        }

        if (isset($params['translate']))
        { try
            {
                $this->setTranslate(new WordValueObject($params['translate']));
            }
            catch (\TypeError $exception){} }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['word' => $this->word,
            'translate' => $this->translate
        ];
    }

}