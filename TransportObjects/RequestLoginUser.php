<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-15
 * Time: 22:08
 */

namespace SDA\Rafal\TransportObjects;

use mysql_xdevapi\Exception;
use SDA\Rafal\Interfaces\TransportObjectsInterface;
use SDA\Rafal\ValueObjects\UserNameValueObject;

class RequestLoginUser implements TransportObjectsInterface
{
    /**
     * @var UserNameValueObject
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * @return UserNameValueObject
     */
    public function getLogin(): UserNameValueObject
    {
        return $this->login;
    }

    /**
     * @param UserNameValueObject $login
     * @return RequestLoginUser
     */
    public function setLogin(UserNameValueObject $login): RequestLoginUser
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return RequestLoginUser
     */
    public function setPassword(string $password): RequestLoginUser
    {
        $this->password = $password;
        return $this;
    }

    public function isValid(): bool
    {
        $status = true;

        if ( (empty($this->login)) || (empty($this->password)) )
        {
            $status = false;
        }

        return $status;
    }

    /**
     * @param array $params
     * @return TransportObjectsInterface
     */
    public function prepareFromArray(array $params): TransportObjectsInterface
    {
        if (isset($params['login']))
        {
            try
            {
                $this->setLogin(new UserNameValueObject($params['login']));
            }
            catch (\TypeError $exception){}
        }

        if (isset($params['password']))
            $this->setPassword($params['password']);

        return $this;
    }

    public function toArray(): array
    {
        $array = [];
        $array['login'] = $this->login;
        $array['password'] = $this->password;

        return $array;
    }
}