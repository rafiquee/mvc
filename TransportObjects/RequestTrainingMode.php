<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-05
 * Time: 22:26
 */

namespace SDA\Rafal\TransportObjects;


use SDA\Rafal\Interfaces\TransportObjectsInterface;
use SDA\Rafal\ValueObjects\WordValueObject;

class RequestTrainingMode extends RequestWordInsert implements TransportObjectsInterface
{
private $inputTranslate;

    /**
     * @return mixed
     */
    public function getInputTranslate()
    {
        return $this->inputTranslate;
    }

    /**
     * @param WordValueObject $inputTranslate
     * @return RequestTrainingMode
     */
    public function setInputTranslate(WordValueObject $inputTranslate): RequestTrainingMode
    {
        $this->inputTranslate = $inputTranslate;
        return $this;
    }

    public function isValid(): bool
    {
        $status = true;
        parent::isValid();
        if(empty($this->getInputTranslate())) $status = false;

        return $status;
    }

    /**
     * @param array $params
     * @return TransportObjectsInterface
     */
    public function prepareFromArray(array $params): TransportObjectsInterface
    {

        if (isset($params['inputTranslate']))
        { try
        {
            $this->setInputTranslate(new WordValueObject($params['inputTranslate']));
        }
        catch (\TypeError $exception){}
        }
        parent::prepareFromArray($params);
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
       $array = parent::toArray();
       $array['inputTranslate'] = $this->inputTranslate;
       return $array;
    }
}