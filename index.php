<?php
session_start();
require_once('autoloader.php');

use SDA\Rafal\Lib\EntityManager;
use SDA\Rafal\TransportObjects\RequestWordInsert;
use SDA\Rafal\Entities\RequestWordInsertEntity;
use SDA\Rafal\ValueObjects\WordValueObject;
use SDA\Rafal\Lib\MySQLmanager;

if (!isset($_SESSION['login'])) { header("Location: /mvc/login.php"); }
$login = $_SESSION['login'];

if (!isset($_GET["page"]))
    $_GET["page"] = 0;

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{

    if (HtmlFormValidator())
    {
        $WordToInsert = new RequestWordInsert();
        $WordToInsert->prepareFromArray($_POST);
        if ($WordToInsert->isValid()) insertWordToDB($WordToInsert);
    }

}

    function insertWordToDB(RequestWordInsert $WordToInsert) :void
    {

        $entity = new RequestWordInsertEntity();
        $entity->setWord($WordToInsert->getWord());
        $entity->setTranslate($WordToInsert->getTranslate());
        $entityManager = (new EntityManager())->insert($WordToInsert);

        if (!$entityManager) {
            die('Error: write in to data base problem');
        }
        unset($_POST);

    }

    //all records from db to one array
    function fetchAll(): array
    {

        $login = $_SESSION['login'];
        $Manager = new MySQLmanager();
        $result = $Manager->fetch_all("SELECT word, translate FROM $login");

        if (!empty($result)) return $result; else return [''];

    }

    function HtmlFormValidator(): bool
    {

        $statusWord = [];
        $statusTranslate = [];

        $word = $_POST['word'];
        $translate = $_POST['translate'];

        if (empty($word)) $statusWord[] = 'Error: word field is empty';
        if (empty($translate)) $statusTranslate[] = 'Error: translate field is empty';

        if (isset ($_POST['word']) && isset($_POST['translate']))
        {
            try { new WordValueObject($word); } catch (TypeError $exception) { $statusWord[] = $exception->getMessage(); }
            try { new WordValueObject($translate); } catch (TypeError $exception) { $statusTranslate[] = $exception->getMessage(); }
        }

        //array[0] give us first error reported
        if (isset($statusWord[0])) { $_SESSION['word_status'] = $statusWord[0]; }
        if (isset($statusTranslate[0])) { $_SESSION['translate_status'] = $statusTranslate[0]; }
        if (empty($statusWord[0]) && empty($statusTranslate[0])) return true;
        else if (!empty($statusWord[0]) || !empty($statusTranslate[0])) return false;

    }

    //function that displays a certain number of words on the page
    function printWords(int $page): void
    {

        $words = fetchAll();
        $words = array_reverse($words);
        $howManyResults = 7;
        $tempArray = [];

        for ($i = 0; $i < count($words); $i++)
            $tempArray[$i] = $words[$i][0] . ' - ' . $words[$i][1];

        $wordsToDisplay = array_chunk($tempArray, $howManyResults);

        foreach ($wordsToDisplay as $pageToDisplay => $results)
        {

            foreach ($results as $key => $value)

                if($pageToDisplay == $page)
                {
                    $final = explode(" - ", $value);

                    echo '<div class="wordsBorder"><p class="textBorder"><b>' . $final[0] . '</b> - '. $final[1].'</p></div>';
                }else
                    echo '';
        }
        //how many page in paginator
        $_SESSION['howManyPages'] = ((int)(count($words) - 1)/$howManyResults);

    }

    //routing permission
    function actualPage()
    {

        if (isset($_GET['page']))
        {
            $allowed_pages = [];
            $page = filter_var($_GET['page'], FILTER_SANITIZE_STRING);

            for ($i=0; $i <= $_SESSION['howManyPages']; ++$i) { array_push($allowed_pages, $i); }

            if ($page == 0) printWords(0);
            if (!empty($page)) echo (!in_array($page, $allowed_pages)) ? 'Error: sorry, this page not exist' : printWords($page);
        }
        else

            printWords(0);

    }

    //paginator
    function linkPages()
    {

        for ($i=0; $i<=$_SESSION['howManyPages']; $i++) echo '<a id="link'.$i.'" href="index.php?page='.$i.'"> '.($i+1).' </a>';

    }

?>

<!DOCTYPE html>
<html>

<head>
    <title>LOGO - Hi <?php echo $login; ?>!</title>
    <link rel="stylesheet" href="Public/css/style.scss.min.css">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="up"><?php echo '.::'.$login.'::.' ?><div id="menuButtons"><img id = "search" class = "search" src="Public/img/lupa.png" alt = "searching button"><img id = "dict" class = "dict" src="Public/img/dict.png" alt = "dictionary button"><img id = "stoper" class = "stoper" src="Public/img/stoper.png" alt = "training button"><img id = "logout" class = "logout" src="Public/img/logout.png" alt = "logout button"></div></div>
        <div class="left"><img class="navi_dict" id="nav1" src="Public/img/nav_left.png"/></div>
        <div class="content" id="jcontent">
            <?php
            actualPage();
            ?>
            <div class="link_index" id="link_index">
            <?php
            linkPages();
            ?>
            </div>
        </div>
        <div class="right"><img class="navi_dict" id="nav2" src="Public/img/nav_right.png"/></div>
        <div class="inputDb">
            <form method="post" action="index.php">
                <input type="text" id="inputDbWord" name="word" placeholder="word" maxlength="20" size="19" value="<?php echo isset($_POST['word']) ? $_POST['word'] : ''; ?>"/>
                <input type="text" id="inputDbWord" name="translate" placeholder="translate" maxlength="20" size="19" value="<?php echo isset($_POST['translate']) ? $_POST['translate'] : ''; ?>"/>
                <input type="submit" id="inputDbWord" value="save"/>
                <div class="down_text"><?php if (isset($_POST['word'])){ if (isset($_SESSION['word_status'])){ echo $_SESSION['word_status'].'</br>'; unset($_SESSION['word_status']); } } ?> <?php if (isset($_POST['translate'])){ if (isset($_SESSION['translate_status'])){ echo $_SESSION['translate_status']; unset($_SESSION['translate_status']); }} ?></div>
            </form>
        </div>
    </div>
    <div class="footer">Created by rafiquee 2019 &copy;</div>
    <script type="text/javascript" src="Public/js/jquery.min.js"></script>
    <script type="text/javascript">

        function paginatorActualLinkColor(page)
        {

            let id = 'link' + page;
            let color = document.getElementById(id);
            color.style.color = "#cc3ded";

        }

        function paginatorNavigationNext(page)
        {

            max = parseInt(<?php echo $_SESSION['howManyPages'] ?>);

            if (page >= max) {} else { pageUrl = page + 1; window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/index.php?page=" + pageUrl); }

        }

        function paginatorNavigationBack(page)
        {

            if (page < 1) {} else { pageUrl = page - 1; window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/index.php?page=" + pageUrl); }

        }

        $('#nav2').click(function(){ paginatorNavigationNext(<?php echo $_GET['page'] ?>)});
        $('#nav1').click(function(){ paginatorNavigationBack(<?php echo $_GET['page'] ?>)});
        $('#logout').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/logout.php");});
        $('#stoper').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/training.php");});
        $('#search').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/search.php");});
        $('#dict').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/index.php");});

        window.onload = paginatorActualLinkColor( <?php echo $_GET["page"] ?> );

    </script>
</body>
</html>
