<?php
session_start();
require_once('autoloader.php');

use Beeflow\Passwd\Passwd;
use SDA\Rafal\TransportObjects\RequestRegisterUser;
use SDA\Rafal\Lib\EntityManager;
use SDA\Rafal\Lib\MySQLmanager;
use SDA\Rafal\ValueObjects\EmailValueObject;

function passwordVerification($pass)
{

    $passwordPolicy = array(
        'specialCharsCount' => 0,
        'minimumPasswordLength' => 5,
        'lowerCharsCount' => 1,
        'upperCharsCount' => 0,
        'numbersCount' => 1
    );
    $password = new Passwd($passwordPolicy);

    $isPasswordOk = $password->check($pass);
    return (!$isPasswordOk) ? false : true;

}

function register(RequestRegisterUser $user ) :void
{
    $Manager = new MySQLmanager();
    $password = $user->getPassword();

    if (passwordVerification($password))
    {
        $user->setPassword(password_hash($password, PASSWORD_DEFAULT));

        $entityManager = (new EntityManager())->insert($user);  //insertion to data base

        if (!$entityManager)
        {
            die('The registration process has been not completed');
        }
        else if($entityManager)
        {
            $login = $_POST['login'];
            $Manager->executeQuery("CREATE TABLE `mvc`.`$login` ( `id` INT(150) NOT NULL AUTO_INCREMENT , `word` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL , `translate` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL , `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        }
        echo "<h3 style='color: lightgreen'>The registration process has been completed</h3>";
        unset($_POST);

    }else{
        echo "<h3 style='color: red'>The registration process has been not completed</h3>";
    }

}

function HtmlFormValidator() :bool
{

    $statusLogin = [];
    $statusPassword = [];
    $statusEmail = [];
    $statusEmailRepeat = [];

    $login = $_POST['login'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $emailRepeat = $_POST['email_repeat'];

    if (empty($_POST['login'])) $statusLogin[] = 'Error: login field is empty';
    if (empty($_POST['password'])) $statusPassword[] = 'Error: password field is empty';
    if (empty($_POST['email'])) $statusEmail[] = 'Error: email field is empty';
    if (empty($_POST['email_repeat'])) $statusEmailRepeat[] = 'Error: email repeat field is empty';

    //DB level verification
    $Manager = new MySQLmanager();
    if ($result = $Manager->oneRecord("SELECT login FROM users WHERE login = '$login'")){ $statusLogin[] = 'Error: this login is already taken'; }
    if (!passwordVerification($password)) { $statusPassword[] = 'Error: password must be composed by </br> minimum 5 chars and include 1 lower </br> char and 1 number'; }
    if ($result = $Manager->oneRecord("SELECT email FROM users WHERE email = '$email'")){ $statusEmail[] = 'Error: this email is already taken'; } else try { $user = new EmailValueObject($_POST['email']); } catch (TypeError $exception) { $statusEmail[] = $exception->getMessage(); }
    if ($email != $emailRepeat) { $statusEmailRepeat[] = 'Error: this field must be thiseme like emeil';}

    //array[0] give us first error reported
    if (isset($statusLogin[0])) { $_SESSION['login_status'] = $statusLogin[0]; }
    if (isset($statusPassword[0])) { $_SESSION['password_status'] = $statusPassword[0]; }
    if (isset($statusEmail[0])) { $_SESSION['email_status'] = $statusEmail[0]; }
    if (isset($statusEmailRepeat[0])) { $_SESSION['email_repeat_status'] = $statusEmailRepeat[0]; }

    if( isset($statusLogin[0]) || isset($statusPassword[0]) || isset($statusEmail[0])  || isset($statusEmailRepeat[0]) ) return false; else return true;

}

if (($_SERVER['REQUEST_METHOD'] == 'POST'))
{

    if(HtmlFormValidator())
    {
    $user = new RequestRegisterUser();
    $user->prepareFromArray($_POST);

    if ($user->isValid())
        register($user);
    else
        echo "<h3 style='color: red'>The registration process has been not completed</h3>";
    }
}

?>

<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>MVC - register form</title>
    <meta name="register form" content="mvc register">
    <meta name="Rafał Sokołowski" content="SitePoint">

    <link rel="stylesheet" href="Public/css/styleForm.css">

</head>

<body>
<div>
    <div class="containerRegister">
        <form action="register.php" id="form" method="post"><img style=" margin-bottom: 15px; margin-top: 10px;" src="Public/img/login.png">
            <div id="txt">Login</br>
                <input type="text" name="login" id="input" value="<?php echo isset($_POST['login']) ? $_POST['login'] : ''; ?>">
                <div class="status" id="status"><?php  if (isset($_POST['login']) && isset($_SESSION['login_status'])) { echo $_SESSION['login_status']; unset($_SESSION['login_status']); } ?></div>
            </div>
            <div id="txt">Password</br>
                <input type="password" name="password" id="input" value="<?php echo isset($_POST['password']) ? $_POST['password'] : ''; ?>">
                <div class="status" id="status"><?php if (isset($_POST['password']) && isset($_SESSION['password_status'])) { echo $_SESSION['password_status']; unset($_SESSION['password_status']); } ?></div>
            </div>
            <div id="txt">Email</br>
                <input type="text" name="email" id="input" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
                <div class="status" id="status"><?php if (isset($_POST['email']) && isset($_SESSION['email_status'])) { echo $_SESSION['email_status']; unset($_SESSION['email_status']); } ?></div>
            </div>
            <div id="txt">Repeat email</br>
                <input type="text" name="email_repeat" id="input" value="<?php echo isset($_POST['email_repeat']) ? $_POST['email_repeat'] : ''; ?>">
                <div class="status" id="status"><?php if (isset($_POST['email_repeat']) && isset($_SESSION['email_repeat_status'])) { echo $_SESSION['email_repeat_status']; unset($_SESSION['email_repeat_status']); } ?></div>
            </div>
            <div>
                <input type="submit" value="register" id="button">
            </div>
            <div id="link"><a href="login.php">Login</a> </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="Public/js/jquery.min.js"></script>

<script>
</script>
</body>
</html>


