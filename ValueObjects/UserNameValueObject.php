<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 09:28
 */

namespace SDA\Rafal\ValueObjects;


use SDA\Rafal\CommonObjects\CommonValueObjects;
use SDA\Rafal\Interfaces\ValueObjectInterface;

class UserNameValueObject extends CommonValueObjects implements ValueObjectInterface
{

    public function __construct(string $userName)
    {
        if (strlen($userName) < 5 || strlen($userName) > 31)
        {
            throw new \TypeError('user name must have 5 to 31 characters');
        }

        if (!preg_match('/^[A-Za-z][A-Za-z0-9]{4,31}$/', $userName))
        {
            throw new \TypeError('username contains invalid characters');
        }

        return $this->value = $userName; //value is a properties of abstract class CommonValueObjects
    }

    public function get()
    {
        return $this->value; //in constructor value is assigned
    }

    /**
     * @return int
     * @throws \TypeError
     */
    public function toInt(): int
    {
        throw new \TypeError('username must contain characters');
    }

}