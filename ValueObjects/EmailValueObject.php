<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 13:21
 */

namespace SDA\Rafal\ValueObjects;


use SDA\Rafal\CommonObjects\CommonValueObjects;
use SDA\Rafal\Interfaces\ValueObjectInterface;

class EmailValueObject extends CommonValueObjects implements ValueObjectInterface
{
    /**
     * EmailValueObject constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \TypeError('email incorrect');
        }

        if (strpos( $email,'#') !== false) {
            throw new \TypeError('email incorrect');
        }

        $this->value = $email; //properties assigned by abstract class CommonValueObjects
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->value;
    }

    public function toInt(): int
    {
        throw new \TypeError('email address must contain valid characters');
    }
}