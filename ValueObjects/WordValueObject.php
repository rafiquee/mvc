<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-22
 * Time: 08:41
 */

namespace SDA\Rafal\ValueObjects;


use SDA\Rafal\CommonObjects\CommonValueObjects;
use SDA\Rafal\Interfaces\ValueObjectInterface;

class WordValueObject extends CommonValueObjects implements ValueObjectInterface
{
    /**
     * WordValueObject constructor.
     * @param string $word
     */
    public function __construct(string $word)
    {

        if (strlen($word) < 1 || strlen($word) > 20)
            return new \TypeError('Error: word must be composed by 1-20 chars');

        if (!preg_match('/^[A-Ząćęłńóśźża-z][A-Ząćęłńóśźża-z0-9\040]{1,20}$/', $word)) {
            throw new \TypeError('word contains invalid characters');
        }

        return $this->value = $word;

    }

    public function get()
    {

        return $this->value;

    }

    /**
     * return int
     * @throws \TypeError
     */
    public function toInt(): int
    {

        return new \TypeError('Error: enter valid word');

    }
}