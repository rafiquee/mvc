<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 16:32
 */

namespace SDA\Rafal\Lib;


class MySQLmanager
{

private $handler;
protected $queryResult;

private static $instance;

    /**
     * MySQLmanager constructor.
     */
    public function __construct()
    {
    $connection = mysqli_init();
    $connection->real_connect('localhost', 'root', 'root', 'mvc', 8888);
    $this->handler = $connection;

    return $this;
    }

    /**
     * @param $sql
     * @return MySQLmanager
     */
    public function executeQuery($sql): MySQLmanager
    {
        $connection = $this->handler;

        if ($result = $connection->query($sql))
        {
            //echo "<h3>the operation was successful</h3>";
            return $this;
        }
        else
        {
            echo '<h3>Error: '. $connection->error .'</h3>';
        }
        $connection->close();
    }

    public function oneRecord($sql)
    {
        $connection = $this->handler;

        if ($result = $connection->query($sql))
        {
            return $result->fetch_row();
        }
    }

    public function fetch_all($sql)
    {

        $connection = $this->handler;

        if ($result = $connection->query($sql))
        {
            return $result->fetch_all();
        }

    }

    /**
     * @return MySQLmanager
     */
    public function getInstance()
    {
        if (empty(self::$instance))
        {
            self::$instance = new MySQLmanager();
            return self::$instance;
        }
    }
}

//Table contain new user
//CREATE TABLE `mvc`.`users` ( `login` VARCHAR(13) NOT NULL , `password` VARCHAR(255) NOT NULL , `email` VARCHAR(25) NOT NULL , `email_repeat` VARCHAR(25) NOT NULL , UNIQUE `login` (`login`)) ENGINE = InnoDB;