<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-20
 * Time: 18:36
 */

namespace SDA\Rafal\Lib\EntityProviders;

use SDA\Rafal\Entities\RequestLoginUserEntity;
use SDA\Rafal\Interfaces\EntityInterface;
use SDA\Rafal\Interfaces\EntityProvidersInterface;
use SDA\Rafal\Interfaces\TransportObjectsInterface;
use SDA\Rafal\TransportObjects\RequestLoginUser;

class RequestLoginUserEntityProviders implements EntityProvidersInterface
{

    public function prepareFromTransportObject(TransportObjectsInterface $TransportObject): EntityInterface
    {
        /**
         * @var RequestLoginUser $requestLoginUser
         */
        $requestLoginUser = $TransportObject;
        $en = new RequestLoginUserEntity();

        $en->setLogin($requestLoginUser->getLogin());
        $en->setPassword($requestLoginUser->getPassword());

        return $en;
    }
}