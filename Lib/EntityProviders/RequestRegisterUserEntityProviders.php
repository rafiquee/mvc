<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 15:11
 */

namespace SDA\Rafal\Lib\EntityProviders;

use SDA\Rafal\Entities\RequestRegisterUserEntity;
use SDA\Rafal\Interfaces\EntityInterface;
use SDA\Rafal\Interfaces\EntityProvidersInterface;
use SDA\Rafal\Interfaces\TransportObjectsInterface;
use SDA\Rafal\TransportObjects\RequestRegisterUser;
use SDA\Rafal\ValueObjects\UserNameValueObject;

class RequestRegisterUserEntityProviders implements EntityProvidersInterface
{

    //funkcja zwraca obiekt, dostosowany do polecenia insert sql w entitymanager;
    public function prepareFromTransportObject(TransportObjectsInterface $TransportObject): EntityInterface
    {
    /** @var RequestRegisterUser $requestRegisterUser */
    $requestRegisterUser = $TransportObject;
    $en = new RequestRegisterUserEntity();

    $en->setLogin($requestRegisterUser->getLogin());
    $en->setPassword($requestRegisterUser->getPassword());
    $en->setEmail($requestRegisterUser->getEmail());

    return $en;
    }

}