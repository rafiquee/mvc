<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-22
 * Time: 08:43
 */

namespace SDA\Rafal\Lib\EntityProviders;


use SDA\Rafal\Entities\RequestWordInsertEntity;
use SDA\Rafal\Interfaces\EntityInterface;
use SDA\Rafal\Interfaces\EntityProvidersInterface;
use SDA\Rafal\Interfaces\TransportObjectsInterface;
use SDA\Rafal\TransportObjects\RequestWordInsert;

class RequestWordInsertEntityProviders implements EntityProvidersInterface
{

    public function prepareFromTransportObject(TransportObjectsInterface $TransportObject): EntityInterface
    {
        /**
         * @var RequestWordInsert $requestWordInsert
         */
        $requestWordInsert = $TransportObject;
        $en = new RequestWordInsertEntity();

        $en->setWord($requestWordInsert->getWord());
        $en->setTranslate($requestWordInsert->getTranslate());

        return $en;
    }
}