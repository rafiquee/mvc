<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-15
 * Time: 22:31
 */

namespace SDA\Rafal\Lib;


use SDA\Rafal\Interfaces\EntityInterface;
use SDA\Rafal\Interfaces\EntityProvidersInterface;
use SDA\Rafal\Interfaces\TransportObjectsInterface;
use SDA\Rafal\Lib\MySQLmanager;
use SDA\Rafal\Interfaces\ValueObjectInterface;

class EntityManager
{
    /**
     * @var EntityInterface
     */
    private $entity;

    /**
     * @param TransportObjectsInterface $transportObject
     */
    public function prepareEntity (TransportObjectsInterface $transportObject): void
    {
        $namespace = '\\SDA\\Rafal\\Lib\\EntityProviders\\';

        $transportObjectClassName = get_class($transportObject);

        $transportObjectNamespaceElements = explode('\\', $transportObjectClassName);
        end($transportObjectNamespaceElements);
        $entityProviderClassName = $namespace . current($transportObjectNamespaceElements) . 'EntityProviders';

        /** @var EntityProvidersInterface $entityProvider */
        $entityProvider = new $entityProviderClassName();
        //var_dump($entityProvider);
        $entity  = $entityProvider->prepareFromTransportObject($transportObject);

        $this->entity = $entity;
    }

    //register.php
    public function insert (TransportObjectsInterface $transportObject): bool
    {

        $this->prepareEntity($transportObject);

        if (!$this->entity->isValid())
        {
            return false;
        }

        $sql = 'INSERT INTO ' . $this->entity->getTableName() .
            ' (' . implode(',', $this->entity->getFields()) . ') VALUES (';

        $valuesToInsert = [];
        $entityAsArray = $this->entity->toArray();

        foreach ($this->entity->getFields() as $fieldName) {
            if (!isset($entityAsArray[$fieldName])) {
                continue;
            }

            $valuesToInsert[] = $this->getSqlValue($entityAsArray[$fieldName]);
        }

        $sql .= implode(',', $valuesToInsert) . ') ';

        /**
         * @var MySQLmanager $MySqlManager
         */
        $MySqlManager = new MySQLmanager();
        $MySqlManager->executeQuery($sql);

        return true;
    }

    /** login.php
     * @param TransportObjectsInterface $transportObjects
     * @return bool
     */
    public function loginValidator (TransportObjectsInterface $transportObjects) :bool
    {

        $this->prepareEntity($transportObjects);

        if (!$this->entity->isValid())
        {
            return false;
        }

        $SqlManager = new MySQLmanager();
        $login = $this->entity->getLogin();
        $loginDB = $SqlManager->oneRecord("SELECT login FROM users WHERE login='$login'");
        $password = $this->entity->getPassword();
        $passwordHash = $SqlManager->oneRecord("SELECT password FROM users WHERE login='$login'");

        $flag = true;
        (!password_verify($password, $passwordHash[0])) ? $flag = false : '';
        (!$loginDB) ? $flag = false : '';

        return $flag;

    }



    /**
     * ta metoda załatwia sprawę jeśli nadesłana wartość jest obiektem
     * @param $value
     * @return string
     */
    private function getSqlValue ($value)
    {
        if (is_string($value)) {
            return "'" . $value . "'";
        }
        if ($value instanceof ValueObjectInterface) {

            return $this->getSqlValue($value->get());
        }
        return $value;
    }

}
