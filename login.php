<?php
session_start();
require_once('autoloader.php');

use SDA\Rafal\Lib\MySQLmanager;
use SDA\Rafal\TransportObjects\RequestLoginUser;
use SDA\Rafal\Lib\EntityManager;
use SDA\Rafal\Entities\RequestLoginUserEntity;


    function loginFormValidator() :void
    {

        $statusLogin = [];
        $statusPassword = [];
        if (empty($_POST['login'])) $statusLogin[] = 'Error: login field is empty';
        if (empty($_POST['password'])) $statusPassword[] = 'Error: password field is empty';

        $login = $_POST['login'];
        $password = $_POST['password'];
        $Manager = new MySQLmanager();

        if (!$result = $Manager->oneRecord("SELECT login FROM users WHERE login = '$login'")) {
            $statusLogin[] = 'Error: login not registred';

        } else {

            //verify password for this user
            $passwordHash = $Manager->oneRecord("SELECT password FROM users WHERE login='$login'");
            if (!password_verify($password, $passwordHash[0]))
            {
                $statusPassword[] = 'Error: password incorrect';
            }
            else
                proceedToLogin();
        }

        if (isset($statusLogin[0])) $_SESSION['login_status'] = $statusLogin[0];
        if (isset($statusPassword[0])) $_SESSION['password_status'] = $statusPassword[0];

    }

    function proceedToLogin()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {

            $user = new RequestLoginUser();
            $user->prepareFromArray($_POST);

            if ($user->isValid())
                login($user);

        }
    }

    function login(RequestLoginUser $user): void
    {

        $entity = new RequestLoginUserEntity();
        $entity->setPassword($user->getPassword());
        $entity->setLogin($user->getLogin());

        $EntityManager = new EntityManager();
        if ($EntityManager->loginValidator($user)) {

            $_SESSION['isLogged'] = true;
            $_SESSION['login'] = $_POST['login'];

                header("Location: /mvc/index.php");
        }
        else
            {

                echo "<h3 style='color: red'>Error: The login process has been not completed</h3>";

        }
    }

    if (isset($_POST['login']) && isset($_POST['password'])) loginFormValidator();

?>

<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>MVC - login form</title>
    <meta name="login form" content="mvc login">
    <meta name="Rafał Sokołowski" content="SitePoint">

    <link rel="stylesheet" href="Public/css/styleForm.css">

</head>

<body>
<div>
    <div class="container"><img style="margin-left: 95px; margin-bottom: 15px; margin-top: 10px;" src="Public/img/login.png">
        <form id="form" action="login.php" method="post">
            <div>Login</br>
                <input type="text" name="login" id="input" value="<?php echo isset($_POST['login']) ? $_POST['login'] : ''; ?>">
                <div class="status"><?php if (isset($_POST['login'])){ if (isset($_SESSION['login_status'])){ echo $_SESSION['login_status']; unset($_SESSION['login_status']); }} else echo ''; ?></div>
            </div>

            <div>Password
                <input type="password" name="password" id="input" value="<?php echo isset($_POST['password']) ? $_POST['password'] : ''; ?>">
                <div class="status"><?php if (isset($_POST['password'])){ if (isset($_SESSION['password_status'])){ echo $_SESSION['password_status']; unset($_SESSION['password_status']); }} else echo ''; ?></div>
            </div>

            <div>
                <input id="button" type="submit" value="login">
            </div>
            <div id="link"><a href="register.php">Registration</a> </div>
        </form>

</div>

</div>

<script type="text/javascript" src="Public/js/jquery.min.js"></script>

<script>

    //window.onload = changeStatus();

</script>
</body>
</html>