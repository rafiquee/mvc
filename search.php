<?php
session_start();
require_once('autoloader.php');

use SDA\Rafal\Lib\MySQLmanager;

if (!isset($_SESSION['login'])) { header("Location: /mvc/login.php"); }
if (isset($_SESSION['login'])) $login = $_SESSION['login'];



function searchInDb()
{

    $login = $_SESSION['login'];
    $inputSearch = $_POST['inputTranslate'];
    $Manager = new MySQLmanager();
    $result = $Manager->oneRecord("SELECT word, translate FROM $login WHERE word='$inputSearch' OR translate='$inputSearch'");

    if($result)
        echo  '<b>'.$result[0].'</b> - '.$result[1];
    else
        echo 'the word is not in the dictionary';

}


?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<head>
    <title>LOGO - Hi <?php echo $login; ?>!</title>
    <link rel="stylesheet" href="Public/css/style.scss.min.css">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
</head>

<body>
<div class="container" style="">
    <div class="up"><?php echo '.::'.$login.'::.' ?><div id="menuButtons"><img id = "search" class = "search" src="Public/img/lupa.png" alt = "searching button"><img id = "dict" class = "dict" src="Public/img/dict.png" alt = "dictionary button"><img id = "stoper" class = "stoper" src="Public/img/stoper.png" alt = "training button"><img id = "logout" class = "logout" src="Public/img/logout.png" alt = "logout button"></div></div>
    <div class="content" id="jcontent">
        <div class="searchContent" id="searchContent">
            <img src="Public/img/imgSearch.png"><br/><br/><br/>
            <form class="search" id="search-form" action="search.php" method="post">
                <input name="inputTranslate" id="inputTranslate" size="25" placeholder="search ?" type="text" />
                <input type="submit" id="button"  value="check"/>
            </form>
            <div id="advice_msg"></div>
            <div class="search-results" id="search-results"><?php if(isset($_POST['inputTranslate']) && empty($_POST['inputTranslate'])) echo 'Error: search field is empty'; elseif (isset($_POST['inputTranslate'])) searchInDb(); ?></div>

        </div>
    </div>
</div>
<div class="footer">Created by rafiquee 2019 &copy;</div>

<script type="text/javascript" src="/mvc/Public/js/jquery.min.js"></script>

<script>
    $('#logout').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/logout.php");});
    $('#stoper').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/training.php");});
    $('#search').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/search.php");});
    $('#dict').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/index.php");});
</script>

</body>
</html>