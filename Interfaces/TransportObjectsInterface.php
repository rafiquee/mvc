<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-15
 * Time: 22:15
 */

namespace SDA\Rafal\Interfaces;


interface TransportObjectsInterface
{
//metoda sprawdza czy ustawiony login i haslo nie jest puste
    public function isValid(): bool;

    // metoda sprawdza czy wymagane pola sa ustawione - jesli tak to przekazuje ich wartosci do setterow
    // array $params -> pobierane są z tablicy $_POST w przypadku klas RequestLoginUser i RequestRegisterUser
    public function prepareFromArray(array $params);

    //class RequestLoginUser -> zwraca []tablice wypełnioną wartosciami pobranymi z seterow (ustawionych za pomocą >prepareFromArray<) --> login i password
    //class RequestRegisterUser -> zwraca []tablice wypełnioną wartosciami pobranymi z seterow (ustawionych za pomocą >prepareFromArray<) --> login i password (parent::toArray()) oraz e-mail
    public function toArray(): array;

    //prepareFromArray(json_decode($jsonString,true)); //musi byc true zeby byla tablica;
    //public function prepareFromJson(string $jsonString): TransportObjectInterface;
}