<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-15
 * Time: 22:35
 */

namespace SDA\Rafal\Interfaces;


interface EntityInterface
{
    public function toArray (): array;

    public function getFields (): array;

    public function getTableName (): string;

    public function prepareFromArray (array $array): EntityInterface;

    public function isValid(): bool;

    public function getIdName():string;
}