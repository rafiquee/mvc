<?php
session_start();
require_once('autoloader.php');

use SDA\Rafal\Lib\MySQLmanager;

if (!isset($_SESSION['login'])) { header("Location: /mvc/login.php"); }
if (isset($_SESSION['login'])) $login = $_SESSION['login'];
if (isset($_SESSION['inputTranslate'])) unset($_SESSION['inputTranslate']);

$Manager = new MySQLmanager();
$result = takeRandomRecordFromDb();

function takeRandomRecordFromDb(): array
{

    $login = $_SESSION['login'];
    $Manager = new MySQLmanager();
    $results = $Manager->fetch_all("SELECT * FROM $login");

    return $results;

}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<head>
    <title>LOGO - Hi <?php echo $login; ?>!</title>
    <link rel="stylesheet" href="Public/css/style.scss.min.css">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="up"><?php echo '.::'.$login.'::.' ?><div id="menuButtons"><img id = "search" class = "search" src="Public/img/lupa.png" alt = "searching button"><img id = "dict" class = "dict" src="Public/img/dict.png" alt = "dictionary button"><img id = "stoper" class = "stoper" src="Public/img/stoper.png" alt = "training button"><img id = "logout" class = "logout" src="Public/img/logout.png" alt = "logout button"></div></div>
        <div class="content" id="jcontent">
            <div id="contentTraining">
                <div id="statusNumber"><label id="stat_good"><?php echo '0';  ?></label><label id="stat_bad"><?php  echo '0'; ?></label></div>
                <div id="answer"> <img id="goodImg"  src="Public/img/good.png" alt = "good_icon"><img id="badImg" src="Public/img/bad.png" alt="good_icon"> </div>
                <h1 id="htmlWord"></h1>
                <input name="inputTranslate" id="inputTranslate" type="text" />
                <input type="button" id="button"  value="check"/>
                <input type="button" id="advice" value="?"/>
                <div id="advice_msg"></div>
            </div>
        </div>
    </div>
    <div class="footer">Created by rafiquee 2019 &copy;</div>

    <script type="text/javascript" src="/mvc/Public/js/jquery.min.js"></script>
    <script>

        var randomRecordFromDb = randomRecord();
        window.onload= function(){ document.getElementById('htmlWord').innerHTML = randomRecordFromDb[2]; }

        goodAnswer = 0;
        badAnswer = 0;

        function randomRecord() {

            let allRecords = <?=json_encode($result)?>;
            let allRecordsLength = allRecords.length;
            let randomNumberFromAllRecords = Math.floor(((Math.random() * allRecordsLength) - 1) + 1);
            let randomRecordFromDb = allRecords[randomNumberFromAllRecords];

            return randomRecordFromDb;
        }

        function compare()
        {

            let textInput = document.getElementById('inputTranslate');

            if (textInput.value===randomRecordFromDb[1])
            {
                ++goodAnswer;
                document.getElementById('stat_good').innerHTML = goodAnswer;

                //print new word in html
                let temp = randomRecord();
                let randWord = document.getElementById('htmlWord');
                randWord.innerHTML = temp[2];
                randomRecordFromDb[1] = temp[1];

                //clear advais field
                document.getElementById('advice_msg').innerHTML = '';
                //clear inputTranslate
                document.getElementById('inputTranslate').value = '';
            }
            else if (textInput.value!==randomRecordFromDb[1])
            {
                ++badAnswer;
                document.getElementById('stat_bad').innerHTML = badAnswer;

                //print new word in html
                let temp = randomRecord();
                let randWord = document.getElementById('htmlWord');
                randWord.innerHTML = temp[2];
                randomRecordFromDb[1] = temp[1];

                //clear advais field
                document.getElementById('advice_msg').innerHTML = '';
                //clear inputTranslate
                document.getElementById('inputTranslate').value = '';
            }
        }

        function printAdviceMsg()
        {

            document.getElementById('advice_msg').innerHTML = randomRecordFromDb[1];

        }

        //event action key
        window.addEventListener('keydown', function(event)
        {
            switch (event.keyCode)
            {
                case 13: // enter
                    compare();
                    break;

            }
        }, false);

        $('#button').click(function(){ compare(); });
        $('#advice').click(function(){ printAdviceMsg(); });
        $('#logout').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/logout.php");});
        $('#stoper').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/training.php");});
        $('#search').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/search.php");});
        $('#dict').click(function(){window.location.assign("<?php echo $_SERVER[["HTTP_HOST"]] ?>/mvc/index.php");});

    </script>

</body>
</html>